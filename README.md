# Replica Cache

This module provides a replica database cache backend.

Cache `gets` will attempt to use the replica database connection if available.
Otherwise, the primary database will be used as normal.

Cache `sets` will trigger the 'replica kill switch' forcing the primary database
to be used for the remainder of the current request.

## Setup

Specify the cache backend in `settings.php`:

```
$settings['cache']['default'] = 'cache.backend.replica';
```

## Configuration

You may wish to adjust the `maximum_replication_lag` setting from it's default of
`300` seconds.

```
$settings['maximum_replication_lag'] = 5;
```

