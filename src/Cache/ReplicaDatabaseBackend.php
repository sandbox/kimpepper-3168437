<?php

namespace Drupal\replica_cache\Cache;

use Drupal\Core\Cache\CacheTagsChecksumInterface;
use Drupal\Core\Cache\DatabaseBackend;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\ReplicaKillSwitch;

/**
 * Provides a replica database cache backend.
 *
 * @ingroup cache
 */
class ReplicaDatabaseBackend extends DatabaseBackend {

  /**
   * The replica database.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $replica;

  /**
   * The replica kill switch.
   *
   * @var \Drupal\Core\Database\ReplicaKillSwitch
   */
  protected $replicaKillSwitch;

  /**
   * Kill switch triggered.
   *
   * @var bool
   */
  protected $killSwitchTriggered;

  /**
   * ReplicaDatabaseBackend constructor.
   *
   * @param \Drupal\Core\Database\Connection $replica
   *   The replica database.
   * @param \Drupal\Core\Database\ReplicaKillSwitch $replicaKillSwitch
   *   The replica kill switch.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Cache\CacheTagsChecksumInterface $checksum_provider
   *   The cache tags checksum provider.
   * @param string $bin
   *   The cache bin for which the object is created.
   * @param null $max_rows
   *   (optional) The maximum number of rows that are allowed in this cache bin
   *   table.
   */
  public function __construct(Connection $replica, ReplicaKillSwitch $replicaKillSwitch, Connection $connection, CacheTagsChecksumInterface $checksum_provider, $bin, $max_rows = NULL) {
    parent::__construct($connection, $checksum_provider, $bin, $max_rows);
    $this->replica = $replica;
    $this->replicaKillSwitch = $replicaKillSwitch;
    $this->killSwitchTriggered = FALSE;
  }

  public function getMultiple(&$cids, $allow_invalid = FALSE) {
    $cid_mapping = [];
    foreach ($cids as $cid) {
      $cid_mapping[$this->normalizeCid($cid)] = $cid;
    }
    // When serving cached pages, the overhead of using ::select() was found
    // to add around 30% overhead to the request. Since $this->bin is a
    // variable, this means the call to ::query() here uses a concatenated
    // string. This is highly discouraged under any other circumstances, and
    // is used here only due to the performance overhead we would incur
    // otherwise. When serving an uncached page, the overhead of using
    // ::select() is a much smaller proportion of the request.
    $result = [];
    try {
      $result = $this->replica->query('SELECT cid, data, created, expire, serialized, tags, checksum FROM {' . $this->connection->escapeTable($this->bin) . '} WHERE cid IN ( :cids[] ) ORDER BY cid', [':cids[]' => array_keys($cid_mapping)]);
    }
    catch (\Exception $e) {
      // Nothing to do.
    }
    $cache = [];
    foreach ($result as $item) {
      // Map the cache ID back to the original.
      $item->cid = $cid_mapping[$item->cid];
      $item = $this->prepareItem($item, $allow_invalid);
      if ($item) {
        $cache[$item->cid] = $item;
      }
    }
    $cids = array_diff($cids, array_keys($cache));
    return $cache;
  }

  /**
   * {@inheritdoc}
   */
  public function setMultiple(array $items) {
    $this->triggerKillSwitch();
    parent::setMultiple($items);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteMultiple(array $cids) {
    $this->triggerKillSwitch();
    parent::deleteMultiple($cids);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteAll() {
    $this->triggerKillSwitch();
    parent::deleteAll();
  }

  /**
   * {@inheritdoc}
   */
  public function invalidateMultiple(array $cids) {
    $this->triggerKillSwitch();
    parent::invalidateMultiple($cids);
  }

  /**
   * {@inheritdoc}
   */
  public function invalidateAll() {
    $this->triggerKillSwitch();
    parent::invalidateAll();
  }

  /**
   * Triggers the kill switch if it hasn't been triggered yet.
   */
  protected function triggerKillSwitch(): void {
    if (!$this->killSwitchTriggered) {
      $this->replicaKillSwitch->trigger();
      $this->killSwitchTriggered = TRUE;
    }
  }

}
