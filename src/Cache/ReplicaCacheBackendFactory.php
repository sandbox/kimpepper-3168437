<?php

namespace Drupal\replica_cache\Cache;

use Drupal\Core\Cache\CacheTagsChecksumInterface;
use Drupal\Core\Cache\DatabaseBackendFactory;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\ReplicaKillSwitch;
use Drupal\Core\Site\Settings;

/**
 * Provides a factory for replica database cache.
 *
 * @ingroup cache
 */
class ReplicaCacheBackendFactory extends DatabaseBackendFactory {

  /**
   * The replica database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $replica;

  /**
   * The replica kill switch.
   *
   * @var \Drupal\Core\Database\ReplicaKillSwitch
   */
  protected $replicaKillSwitch;

  /**
   * Constructs the DatabaseBackendFactory object.
   *
   * @param \Drupal\Core\Database\Connection $replica
   *   The replica database connection.
   * @param \Drupal\Core\Database\ReplicaKillSwitch $replicaKillSwitch
   *   The replica kill switch.
   * @param \Drupal\Core\Database\Connection $connection
   *   Database connection
   * @param \Drupal\Core\Cache\CacheTagsChecksumInterface $checksum_provider
   *   The cache tags checksum provider.
   * @param \Drupal\Core\Site\Settings|null $settings
   *   (optional) The site settings.
   *
   */
  public function __construct(Connection $replica, ReplicaKillSwitch $replicaKillSwitch,  Connection $connection, CacheTagsChecksumInterface $checksum_provider, Settings $settings = NULL) {
    parent::__construct($connection, $checksum_provider, $settings);
    $this->replica = $replica;
    $this->replicaKillSwitch = $replicaKillSwitch;
  }

  /**
   * Gets DatabaseBackend for the specified cache bin.
   *
   * @param $bin
   *   The cache bin for which the object is created.
   *
   * @return \Drupal\Core\Cache\DatabaseBackend
   *   The cache backend object for the specified cache bin.
   */
  public function get($bin) {
    $max_rows = $this->getMaxRowsForBin($bin);
    return new ReplicaDatabaseBackend($this->replica, $this->replicaKillSwitch, $this->connection, $this->checksumProvider, $bin, $max_rows);
  }
}
